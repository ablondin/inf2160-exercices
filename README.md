# Paradigmes de programmation

Ce dépôt contient les énoncés des laboratoires du cours INF2160 Paradigmes de
programmation, enseigné à l'UQAM.

* [Quelques fichiers de configuration](config/README.md)
* [Labo 1](labo01/README.md)
* [Labo 2](labo02/README.md)
* [Labo 3](labo03/README.md)
* [Labo 4](labo04/README.md)
* [Labo 5](labo05/README.md)
* [Labo 6](labo06/README.md)
* [Labo 7](labo07/README.md)
* [Labo 8](labo08/README.md)
* [Labo 9](labo09/README.md)
* [Labo 10](labo10/README.md)
