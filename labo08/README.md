# Laboratoire 8: Introduction à Prolog

## 1 - Introduction

Considérez la base de faits décrite dans le fichier
[nourriture.pl](nourriture.pl) de ce dépôt. Lisez-le pour bien vous
familiariser avec son contenu.

Lancez ensuite l'interpréteur SWI-Prolog avec la commande
```
swipl nourriture.pl
```
Cela permet de charger les faits déclarés dans le fichier.

Interrogez cette base de connaissance pour avoir les informations suivantes:

1. Est-ce que le navet est sucré?
2. Quels sont les légumes déclarés dans la base?
3. De quelle couleur est une pomme?
4. Quels sont les fruits sucrés?
5. Quels sont les aliments de couleur orange et de goût sucré?
6. Quels légumes sont verts?

N'hésitez pas à modifier la base de faits pour effectuer d'autres tests. Notez
que pour recharger une base de fait, il suffit d'entrer (sans oublier le point
final):
```prolog
?- make.
```

## 2 - Listes

Proposez une implémentation Prolog des prédicats suivants.

### 2.1 - `concat`

Définissez un prédicat `concat(L1, L2, L)`, qui indique si `L` est la liste
obtenue en concaténant `L1` et `L2`.

On s'attend donc au comportement suivant:

```prolog
?- concat([], [], L).
L = [].

?- concat([1,2,3], [4,5], L).
L = [1, 2, 3, 4, 5].

?- concat([1,2],[2,1], L).
L = [1, 2, 2, 1].

?- concat([1,2,3], L, [1,2,3,4,5]).
L = [4, 5].

?- concat([1,2,3], L, [4,5]).
false.
```

### 2.2 - `renverse`

Définissez un prédicat `renverse(L1, L2)`, qui indique si `L1` est la liste
obtenue de `L2` en renversant l'ordre dans lequel les éléments sont écrits.

*Indice*: N'hésitez pas à utiliser le prédicat `concat` défini plus haut.

```prolog
?- renverse([], []).
true.

?- renverse([1,2],[2,1]).
true.

?- renverse([alice], [alice]).
true.

?- renverse([alpha, beta], [alpha, beta]).
false.
```

### 2.3 - `palindrome`

Définissez un prédicat `palindrome(L)`, qui indique si `L` est une liste
palindromique, c'est-à-dire que le premier élément est égal au dernier, le
deuxième à l'avant-dernier, etc.

*Indice*: N'hésitez pas à utiliser le prédicat `concat` défini plus haut.

On s'attend donc au comportement suivant:
```prolog
?- palindrome([]).
true.

?- palindrome([alice]).
true.

?- palindrome([1,2,3]).
false.

?- palindrome([1,2,1]).
true.

?- palindrome([alpha, beta, beta, alpha]).
true.
```

## 3 - Modélisation

En géométrie de base, il existe différents types d'objets en 2D. Par exemple,

- Un *cercle*;
- Un *polygone*, qui a un nombre arbitraire de côtés (au moins 3);
- Un *triangle*, qui est un polygone de 3 côtés;
- Un *quadrilatère*, qui est un polygone de 4 côtés;

Parmi les quadrilatères, on peut raffiner le type également:

- Un *trapèze*, qui a au moins 1 paire de côtés parallèles;
- Un *parallélogramme*, qui a 2 paires de côtés parallèles;
- Un *rectangle*, qui a 4 angles droits;
- Un *losange*, qui a 4 côtés de même longueur et 2 paires de côtés parallèles;
- Un *carré*, qui a 4 côtés de même longueur et 4 angles droits;

Supposons que les atomes qui nous intéressent sont
```prolog
cercle
polygone
triangle
quadrilatere
trapeze
parallelogramme
rectangle
losange
carre
```
En utilisant des règles bien choisies, proposez un modèle pour ces différents
objets qui supporte les requêtes suivantes:
```prolog
est_cercle(X)
est_polygone(X)
est_triangle(X)
est_quadrilatere(X)
est_trapeze(X)
est_parallelogramme(X)
est_rectangle(X)
est_losange(X)
```
N'oubliez pas qu'un carré est un cas particulier de rectangle et de losange.
Par contre, un carré n'est pas un cercle.

Évidemment, il est possible de définir ces fonctions en faisant tous les cas
possibles, mais il existe une solution plus intéressante qui permet de faire de
l'inférence. Par exemple, on sait que si `X` est un rectangle, alors c'est
forcément un parallélogramme, un trapèze et un quadrilatère.
