# Laboratoire 4: Types et foncteurs

# 1 - Types algébriques

Récupérez le fichier [Image.hs](Image.hs) disponible dans le dépôt:
```haskell
module Image where

-- | Une couleur représentée par son niveau de rouge, vert et bleu
data Couleur = RGB Int Int Int

-- | Une image
data Image = Image {
    hauteur :: Int,
    largeur :: Int,
    pixels :: [[Couleur]]
}

-- | Indique si la hauteur et la largeur de l'image sont cohérentes avec la
-- matrice de pixels
estValide :: Image -> Bool
estValide = error "À compléter"

-- | Retourne une image de dimensions données dont tous les pixels sont de même
-- couleur
imageUnicolore :: Int     -- Hauteur
               -> Int     -- Largeur
               -> Couleur -- La couleur
               -> Image   -- L'image résultante
imageUnicolore = error "À compléter"

-- | Retourne une image de dimensions données en alternant les couleurs données
-- comme dans un échiquier.
echiquier :: Int     -- Hauteur
          -> Int     -- Largeur
          -> Couleur -- Première couleur
          -> Couleur -- Deuxième couleur
          -> Image   -- L'image résultante
echiquier = error "À compléter"
```
Complétez la définition des fonctions `estValide`, `imageUnicolore` et
`echiquier`.

# 2 - Types récursifs

Considérez les déclarations suivantes, disponibles dans [Arbre.hs](Arbre.hs):

```haskell
module Arbre where

-- | Un arbre binaire de recherche classique
data ABR a = ABRVide | Noeud a (ABR a) (ABR a)
    deriving (Show)

-- | Insertion
inserer :: Ord a => a -> ABR a -> ABR a
inserer x ABRVide = Noeud x ABRVide ABRVide
inserer x (Noeud y g d)
    | x == y = Noeud x g d
    | x < y  = Noeud y (inserer x g) d
    | x > y  = Noeud y g (inserer x d)

-- | Insertions multiples
insererPlusieurs :: Ord a => ABR a -> [a] -> ABR a
insererPlusieurs = foldl (flip inserer)

-- | Appartenance
appartient :: Ord a => a -> ABR a -> Bool
appartient _ ABRVide = False
appartient x (Noeud y g d)
    | x == y = True
    | x < y  = appartient x g
    | x > y  = appartient x d

-- | Nombre de noeuds
--
-- Note: Un arbre vide n'est pas un noeud.
nbNoeuds :: ABR a -> Int
nbNoeuds = error "À compléter"

-- | Nombre de feuilles
--
-- Note: Une feuille est un noeud dont les deux sous-arbres sont vides.
nbFeuilles :: ABR a -> Int
nbFeuilles = error "À compléter"

-- | Hauteur
--
-- Note: Un arbre vide est de hauteur 0, alors qu'une feuille est de hauteur 1.
hauteur :: ABR a -> Int
hauteur = error "À compléter"

-- | Est-ce que l'arbre est équilibré?
--
-- Un arbre est équilibré si la différence de hauteur entre *chacun* de ses
-- sous-arbres gauche et droite diffère d'au plus 1.
estEquilibre :: ABR a -> Bool
estEquilibre = error "À compléter"
```

Proposez une implémentation pour chacune des fonctions ci-haut.

# 3 - Foncteurs

Considérez les déclarations suivantes

```haskell
-- Un vecteur 3D générique
data Vecteur a = Vecteur a a a

-- Une image
data Image a = Image {
    hauteur :: Int,
    largeur :: Int,
    pixels :: [[a]]
}
```

Proposez une implémentation de la classe `Functor` pour les constructeurs de
types `Vecteur` et `Image`. On s'attend au comportement suivant:

```haskell
Prelude> fmap (+3) (Vecteur 2 5 1)
Vecteur 5 8 4

Prelude> let image = Image 2 3 [[0,1,1],[1,0,1]]
Prelude> image
Image {hauteur = 2, largeur = 3, pixels = [[0,1,1],[1,0,1]]}
Prelude> fmap (1-) image
Image {hauteur = 2, largeur = 3, pixels = [[1,0,0],[0,1,0]]}
```
