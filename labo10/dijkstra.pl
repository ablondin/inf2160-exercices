% Graph

edge(1, 2, 7).
edge(1, 3, 9).
edge(1, 6, 14).
edge(2, 3, 10).
edge(2, 4, 15).
edge(3, 4, 11).
edge(3, 6, 2).
edge(4, 5, 6).
edge(5, 6, 9).

arc(From, To, Length) :- edge(From, To, Length).
arc(From, To, Length) :- edge(To, From, Length).

% Dijkstra
shortest_path(From, To, Path, Length) :-
	dijkstra([0-[From]], To, RPath, Length),
	reverse(RPath, Path).

% On est rendus!
dijkstra([Length-[To|RPath] |_], To, [To|RPath], Length) :- !.

% On cherche le meilleur candidat puis on recommence
dijkstra(Visited, To, RPath, Length) :-
  best_candidate(Visited, BestCandidate), 
  dijkstra([BestCandidate|Visited], To, RPath, Length).

% Calcul du meilleur candidat
best_candidate(Paths, BestCandidate) :-
	findall(
		NP,
	  	(	member(Len-[U|Path], Paths),
		    arc(U, V, Dist),
		    \+ is_visited(Paths, V),
	    	NLen is Len + Dist,
	    	NP = NLen-[V,U|Path]
    	),
        Candidates
	),
  	minimum(Candidates, BestCandidate).

% Minimum d'une liste
minimum(L, Min) :-
	keysort(L, [Min|_]).

% Vérifie si un sommet a été visité
is_visited(Paths, U) :-
	memberchk(_-[U|_], Paths).
