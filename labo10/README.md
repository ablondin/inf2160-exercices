# Laboratoire 10: Parcours de graphes

## 1 - Bases de connaissance

Représentez dans une base de connaissance le graphe suivant, tiré de la page
Wikipedia portant sur l'algorithme de Dijkstra:

![](https://upload.wikimedia.org/wikipedia/commons/5/57/Dijkstra_Animation.gif)

Utilisez les structures suivantes:

- `edge(1, 2, 7)` pour indiquer une arête qui lie les sommets `1` et `2` avec
  un poids de `7`.
- `arc(From, To, Length)` pour identifier un arc allant de `From` vers `To`
  avec un poids de `Length`. Notez qu'une arête (*edge*) est non orientée,
  alors qu'un arc l'est.

## 2 - Minimum d'une liste de structures

Nous aimerions pouvoir trier des expressions complexes. Par exemple,
considérons la liste `[0-[3,2], 4-[1]]`. Je vous rappelle qu'en Prolog, la
soustraction n'est pas différente d'un autre foncteur tant qu'elle n'est pas
interprétée comme telle.

Que se passe-t-il si vous entrez l'instruction suivante dans l'interpréteur
Prolog:
```prolog
min_list([0-[3,2], 4-[1]], M).
```

Pour rectifier la situation, nous allons construire notre propre prédicat
`minimum/2` qui permet de calculer le minimum d'une liste même quand
l'expression est complexe.

Dans un premire temps, consultez la documentation du prédicat `keysort/2`:
http://www.swi-prolog.org/pldoc/man?predicate=keysort/2

Ensuite, à l'aide de ce prédicat, proposez l'implémentation d'un prédicat
```prolog
minimum(L, M)
```
qui est vérifié si `M` est le minimum de la liste `L`. Testez ensuite votre
prédicat:
```prolog
minimum([0-[3,2], 4-[1]], M).
```

## 3 - Algorithme de Dijkstra

Consultez le fichier [dijkstra.pl](dijkstra.pl) disponible dans ce dépôt.
Testez-le sur la base de connaissances que vous avez créées à la question 1.
Ensuite, lisez-le bien pour comprendre son fonctionnement.

## 4 - Trace

Faites la trace de l'algorithme de Dijkstra ainsi implémenté à l'aide du
débogueur Prolog. Pour cela, il suffit de lancer
```prolog
?- trace.
```
puis ensuite de lancer l'exécution d'un prédicat, par exemple
```prolog
[trace]  ?- shortest_path(1, 5, P, L).
```
N'hésitez pas à modifier le code pour voir ce qui se passe! S'il vous reste du
temps, testez le débogage sur des programmes différents.
