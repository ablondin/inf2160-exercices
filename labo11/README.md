# Laboratoire 11: Satisfiabilité et révision

## 1 - Casse-tête logique

Dans un examen à choix multiples, on vous propose la question suivante:

```
Quelle réponse est la bonne?

1. Toutes les réponses ci-bas.
2. Aucune réponse ci-bas.
3. Toutes les réponses ci-haut.
4. Au moins une réponse ci-haut.
5. Aucune réponse ci-haut.
6. Aucune réponse ci-haut.
```

En utilisant le solveur de contraintes CLPB de Prolog, identifiez la bonne
réponse.

## 2 - Révision

Révisez un ancien examen disponible à
[http://www.labunix.uqam.ca/~malenfant_b/inf2160/exa1hiv2012b.pdf](http://www.labunix.uqam.ca/~malenfant_b/inf2160/exa1hiv2012b.pdf)

La solution est également disponible ici
[http://www.labunix.uqam.ca/~malenfant_b/inf2160/exa1hiv2012bR.pdf](http://www.labunix.uqam.ca/~malenfant_b/inf2160/exa1hiv2012bR.pdf)
