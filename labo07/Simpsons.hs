module Simpsons where

import qualified Data.Map.Strict as Map
import Data.Set

type Person = String

parents = Map.fromList [("Bart",    ["Homer",   "Marge"]),
                        ("Lisa",    ["Homer",   "Marge"]),
                        ("Maggie",  ["Homer",   "Marge"]),
                        ("Marge",   ["Clancy",  "Jackie"]),
                        ("Homer",   ["Abraham", "Mona"]),
                        ("Herb",    ["Abraham"]),
                        ("Patty",   ["Clancy",  "Jackie"]),
                        ("Selma",   ["Clancy",  "Jackie"]),
                        ("Abraham", ["Orville", "Yuma"]),
                        ("Yuma",    ["Willard", "Theodora"])]

-- | Retourne tous les parents de toutes les personnes données
parents' :: [Person] -> [Person]
parents' = error "À compléter"

-- | Retourne tous les ancêtres d'une personne donnée
--
-- On dit que x est un ancêtre de y si une des deux conditions suivantes est
-- respectée:
--
-- * x est un parent direct de y
-- * x est l'ancêtre d'un parent de y
ancesters :: Person -> [Person]
ancesters = error "À compléter"
