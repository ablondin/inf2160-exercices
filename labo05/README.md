# Laboratoire 5: Modules

L'objectif de ce laboratoire est de compléter le développement d'un module
Haskell qui fournit une structure de données appelée
[Union-find](https://fr.wikipedia.org/wiki/Union-find) ou [Ensembles
disjoints](https://en.wikipedia.org/wiki/Disjoint-set_data_structure).

Il s'agit d'une structure de données très utile qui permet de représenter une
partition d'un ensemble fini. Elle offre les trois opérations de base suivantes
(en utilisant les signatures en Haskell):

```haskell
-- Crée une partition à partir d'une liste
makePartition :: Ord a => [a] -> Partition a

-- Retourne le représentant d'un élément donné dans la partition
representative :: Ord a => Partition a -> a -> a

-- Fusionne les parties d'une partition contenant deux éléments donnés
merge :: Ord a => a -> a -> Partition a -> Partition a
```

D'autres opérations secondaires peuvent facilement être implémentées à partir
des trois opérations de base:

```haskell
-- Retourne la partie qui contient un élément donné dans une partition
part :: Ord a => Partition a -> a -> [a]

-- Retourne la liste de toutes les parties d'une partition
parts :: Ord a => Partition a -> [[a]]
```

Une implémentation de cette structure est donnée dans le fichier
[Partition.hs](Partition.hs). Comme vous n'avez que deux heures pour compléter
le laboratoire, elle a été complètement implémentée, mais il reste un certain
travail à faire.

## 1 - Génération de la documentation

Lorsqu'on développe un module qu'on souhaite éventuellement partager ou
réutiliser, il est pratique d'utiliser un Makefile, pour automatiser certaines
tâches. Ajoutez un Makefile qui permet de générer la documentation à l'aide de
la commande `make doc`.

En Haskell, l'outil principal qui permet de générer la documentation s'appelle
[Haddock](https://www.haskell.org/haddock/).  En particulier, il suffit
d'entrer la commande
```sh
haddock -o <dossier> --html <fichier.hs>
```
pour générer la documentation du module `<fichier.hs>` dans le répertoire
`<dossier>`.

Vous pouvez visualiser le résultat de la commande en ouvrant le fichier
`index.html` du répertoire qui contient la documentation (typiquement, ce
répertoire s'appelle `doc/`).

Je vous recommande également d'ajouter une cible `clean` qui permet de
supprimer le répertoire de documentation.

## 2 - Documentation des fonctions

Dans un deuxième temps, familiarisez-vous les différentes fonctions du module
afin de vous assurer de bien comprendre son fonctionnement.

Une fois que vous avez bien compris ce qui se passe, améliorez la documentation
du module comme suit:

1. Si des cas spéciaux ou des précisions sont nécessaires pour expliquer le
   comportement de certaines fonctions (je les ai documentées très brièvement),
   indiquez-les dans la *docstring* de la fonction.
2. Proposez au moins deux tests par fonction et ajoutez-les dans les
   *docstrings* des fonctions en respectant la syntaxe du module
   [Doctest](https://github.com/sol/doctest). Notez qu'il s'agit de la même
   syntaxe qui est utilisée dans le premier travail pratique.
3. Au besoin, si vous trouvez qu'une signature n'est pas appropriée, n'hésitez
   pas à la modifier (par exemple en inversant l'ordre des arguments). Aussi,
   si vous trouvez une erreur d'implémentation ou si vous souhaitez un
   comportement différent dans certaines situations, vous êtes libres de
   modifier l'implémentation courante.

Ne négligez pas cette étape, qui est aussi importante que l'implémentation
elle-même des fonctions. Il s'agit aussi d'une excellente préparation pour
l'examen intra.

## 3 - Ajout d'une fonction

Ajoutez une fonction
```haskell
areInSamePart :: Ord a => Partition a -> a -> a -> Bool
```
qui retourne `True` si et seulement si les deux éléments donnés sont dans la
même partie.

N'oubliez pas de documenter la fonction et de lui ajouter un test.

## 4 - Exportation de fonctions

Lorsqu'on souhaite rendre public un module, certaines fonctions doivent être
exportées, alors que d'autres sont utilisées seulement à l'interne (c'est un
principe de base appelé *encapsulation*).

Dans Haskell, les types de données et les fonctions qu'on souhaite exporter
doivent être précisées dans l'en-tête du fichier. Plus précisément, on utilise
la syntaxe
```haskell
module Partition (
    -- On met ici des commentaires qui expliquent les fonctions
    -- Puis on met la liste des fonctions qu'on veut exporter, séparées par des virgules
) where
```

Je vous encourage à consulter un exemple de documentation complet d'une en-tête
de module à
http://haskell-haddock.readthedocs.io/en/latest/markup.html#documentation-structure-examples
pour plus d'informations.

Proposez une interface publique pour ce module. Elle devrait minimalement
respecter les contraintes suivantes:

- Les fonctions `makePartition`, `representative`, `part`, `parts`, `merge`
  devraient être exportées;
- Le type de données `Partition a` ne devrait pas être exporté. Il est donc
  encapsulé et on force l'utilisateur à passer par la fonction `makePartition`.

## 5 - Utilisation du module `Partition`

Une utilisation simple du module `Partition` consiste à construire un
labyrinthe [parfait](https://en.wikipedia.org/wiki/Maze_solving_algorithm).
Il suffit de construire le labyrinthe de la façon suivante:

1. Tout d'abord, on crée une grille rectangulaire dans laquelle chaque case du
   labyrinthe est entourée par des murs. On crée en parallèle une partition
   triviale, dont les éléments sont les cases du labyrinthe, chaque case étant
   initialement seule dans sa propre partie.
2. Ensuite, on choisit deux pièces adjacentes par un mur. En Haskell, pour
   utiliser des valeurs aléatoires, il faut passer par les entrées et sorties
   (que nous n'avons pas encore vues). Par conséquent, vous pouvez choisir les
   deux pièces comme vous voulez, pour simplifier cette partie, sans utiliser
   le hasard.
3. Une fois les deux pièces choisies, on vérifie si elles sont dans la même
   partie de la partition. Si oui, alors on recommence en choisissant une
   nouvelle paire différente de pièces. Évidemment, on s'assure que toute paire
   de pièce puisse être éventuellement choisie (sinon, l'algorithme ne
   terminera jamais).
4. Si les pièces ne sont pas dans une même partie, alors on fusionne les deux
   parties qui les contiennent, puis on "détruit" le mur qui les sépare, pour
   les rendre "connectées".
5. On répète ce processus jusqu'à ce qu'il n'y ait qu'une seule partie dans la
   partition. On obtient alors un labyrinthe parfait.

Proposez un module `Labyrinthe.hs` qui fait appel au module `Partition.hs` pour
générer un labyrinthe parfait. Implémentez au moins les services suivants dans
votre module `Labyrinthe.hs`:

- Créer un labyrinthe trivial de dimensions `h x w`;
- Représenter sous forme de chaîne de caractères un labyrinthe (pour le
  visualiser sur le terminal);
- Créer un labyrinthe parfait de dimensions `h x w`.
