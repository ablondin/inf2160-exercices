module Partition where

import Data.Map (Map)
import qualified Data.Map as Map

-- | A partition of a set of elements.
--
-- A partition is a a list of disjoint lists whose union gives the set of
-- elements.
data Partition a = Partition {
    parent   :: (Map a a),
    numParts :: Int
}

-- | A string representation of a partition
instance (Ord a,Show a) => Show (Partition a) where
    show p = "Partition of " ++ show (numParts p) ++
        " parts: " ++ show (parts p)

-- | Creates a trivial partition from a list
--
-- In a trivial partition, each part contains exactly one element.
makePartition :: Ord a => [a] -> Partition a
makePartition s =
    Partition {
        parent   = m,
        numParts = Map.size m
    }
    where m = Map.fromList $ zip s s

-- | Returns the elements in the partition.
elements :: Ord a => Partition a -> [a]
elements p = Map.keys (parent p)

-- | Returns the representative of an element in a partition.
--
-- A representative can be any element in the same part. In particular, if two
-- elements are in the same part, they always return the same representative.
representative :: Ord a => Partition a -> a -> a
representative p x
    | xParent == x = x
    | otherwise    = representative p xParent
    where xParent = (Map.!) (parent p) x

-- | Returns the part containing a given element in a partition.
part :: Ord a => Partition a -> a -> [a]
part p x = filter ((==x'). representative p) $ elements p
    where x' = representative p x

-- | Returns the list of all parts in a partition.
parts :: Ord a => Partition a -> [[a]]
parts p = [part p x | x <- elements p, x == representative p x]

-- | Merges the parts of two elements.
--
-- If the elements are already in the same part, then the partition is
-- unchanged.
merge :: Ord a => a -> a -> Partition a -> Partition a
merge x y p
    | x' == y'  = p
    | otherwise = Partition{parent=parent',
                            numParts=numParts'}
    where x' = representative p x
          y' = representative p y
          parent' = Map.insert x' y' $ parent p
          numParts' = numParts p - 1
