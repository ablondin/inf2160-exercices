module Image where

import Control.Applicative

-----------
-- Types --
-----------

newtype Image a = Image { getImage :: [[a]] }
type Height = Int
type Width = Int
type Index = (Int,Int)

---------------
-- Functions --
---------------

-- | A string representation of an image.
--
-- >>> show $ makeImage 3 2 (const 1)
-- "[[1,1],[1,1],[1,1]]"
instance Show a => Show (Image a) where
    show = error "À faire"

-- | The classical functor definition.
--
-- >>> fmap (1-) $ Image [[0,1,0],[1,1,0]]
-- [[1,0,1],[0,0,1]]
instance Functor Image where
    fmap = error "À faire"

-- | A zip-like applicative definition.
--
-- >>> pure odd <*> Image [[0,0],[1,0]]
-- [[False,False],[True,False]]
instance Applicative Image where
    pure = error "À faire"
    (<*>) = error "À faire"

-- | Returns an image inflated by a scaling factor
--
-- >>> makeImage 3 2 (const 1)
-- [[1,1],[1,1],[1,1]]
makeImage :: Height -> Width -> (Index -> a) -> Image a
makeImage = error "À faire"

-- | Returns an image inflated by a scaling factor
--
-- >>> checkerImage 3 2 0 1
-- [[0,1],[1,0],[0,1]]
checkerImage :: Height -> Width -> a -> a -> Image a
checkerImage = error "À faire"

-- | Returns the pixel at the given position.
--
-- >>> getPixel (0,2) $ checkerImage 2 3 0 1
-- 0
getPixel :: Index -> Image a -> a
getPixel = error "À faire"

-- | Sets the pixel at the given position.
--
-- >>> setPixel (0,2) 1 $ checkerImage 2 3 0 1
-- [[0,1,1],[1,0,1]]
setPixel :: Index -> a -> Image a -> Image a
setPixel = error "À faire"

-- | Adds two images
--
-- >>> addImages (checkerImage 2 3 0 1) (checkerImage 2 3 1 0)
-- [[1,1,1],[1,1,1]]
addImages :: Num a => Image a -> Image a -> Image a
addImages = error "À faire"

-- | Multiplies two images
--
-- >>> multImages (checkerImage 2 3 0 1) (checkerImage 2 3 1 0)
-- [[0,0,0],[0,0,0]]
multImages :: Num a => Image a -> Image a -> Image a
multImages = error "À faire"

-- | Returns an image inflated by a scaling factor
--
-- >>> checkerImage 2 3 0 1
-- [[0,1,0],[1,0,1]]
-- >>> inflate 2 $ checkerImage 2 3 0 1
-- [[0,0,1,1,0,0],[0,0,1,1,0,0],[1,1,0,0,1,1],[1,1,0,0,1,1]]
inflate :: Int -> Image a -> Image a
inflate = error "À faire"
